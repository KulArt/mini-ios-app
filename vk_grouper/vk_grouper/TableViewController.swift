//
//  TableViewController.swift
//  vk_grouper
//
//  Created by Артур Кулапин on 21.05.2021.
//

import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var feed: Array<Post> = []
    var dataManager = RealmDataManager()
    var vk_manager = VKDelegate()
    
    @IBOutlet weak var tableView: UITableView!
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feed.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Post", for: indexPath)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = feed[indexPath.row].publicText
        cell.textLabel?.lineBreakMode = .byWordWrapping
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func viewDidLoad() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
        if let tmp = vkDelegateReference as? VKDelegate {
            vk_manager.user_id = tmp.user_id
        }
        let array: Array<String> = dataManager.getFeed(id: vk_manager.user_id)
        vk_manager.getFeedFromPublics(publics: array, feed: self.feed, completion:{
            ans, feed in
            self.feed = ans
        })
        do {
            sleep(2)
        }
        super.viewDidLoad()
    }
    
    @IBAction func goBackTapped(_ sender: UIButton) {
        print(feed.count)
        self.viewDidDisappear(true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
