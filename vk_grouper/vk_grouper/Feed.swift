//
//  Feed.swift
//  vk_grouper
//
//  Created by Артур Кулапин on 12.05.2021.
//

import RealmSwift

struct Feed {
    var id: String = ""
    var userId: String = ""
    var publicId: String = ""
    
    func Init() {
    }
}

class RealmFeed: Object {
     
    @objc dynamic var id = ""
    @objc dynamic var userId = ""
    @objc dynamic var publicId = ""
    
    @objc override static func primaryKey() -> String? {
        return "id"
    }
}

extension Feed {
    var realm: RealmFeed {
        let feed = RealmFeed()
        feed.id = id
        feed.userId = userId
        feed.publicId = publicId
        
        return feed
    }
    
    init(realm: RealmFeed) {
        self.id = realm.id
        self.userId = realm.userId
        self.publicId = realm.publicId
    }
}
