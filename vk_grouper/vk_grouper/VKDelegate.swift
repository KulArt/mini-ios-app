//
//  VKDelegate.swift
//  BasicApp
//
//  Created by Артур Кулапин on 12.05.2021.
//

import SwiftyVK
import UIKit

final class VKDelegate: SwiftyVKDelegate {
    
    let appId = "1"
    let scopes: Scopes = [.wall, .friends]
    var user_id = ""
    
    init() {
        VK.setUp(appId: appId, delegate: self)
    }
    
    func vkNeedsScopes(for sessionId: String) -> Scopes {
        return scopes
    }

    func vkNeedToPresent(viewController: VKViewController) {
        // This code works only for simplest cases and one screen applications
        // If you have application with two or more screens, you should use different implementation
        // HINT: google it - get top most UIViewController
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            rootController.present(viewController, animated: true)
        }
    }
    
    func vkTokenCreated(for sessionId: String, info: [String : String]) {
        print("token created in session \(sessionId) with info \(info)")
        self.user_id = info["user_id"]!
    }
    
    func vkTokenUpdated(for sessionId: String, info: [String : String]) {
        print("token updated in session \(sessionId) with info \(info)")
        user_id = info["user_id"]!
    }
    
    func vkTokenRemoved(for sessionId: String) {
        print("token removed in session \(sessionId)")
    }
    
    func getPublicId(name: String, userId: String, dataManager: RealmDataManager, completion: @escaping (String, String, RealmDataManager) -> ()) {
        var ans: String = ""
        VK.API.Groups.getById([.groupIds: name]).onSuccess {
            if let response = try JSONSerialization.jsonObject(with: $0) as? Array<[String: Any]> {
                if let id = response[0]["id"]! as? Int {
                    ans = String(id)
                    DispatchQueue.main.async {
                        completion(ans, userId, dataManager)
                    }
                }
            }
        }.send()
    }
    
    func getFeedFromPublics(publics: Array<String>, feed: Array<Post>, completion: @escaping (Array<Post>, Array<Post>) -> ()) {
        var ans: Array<Post> = []
        var stringPublics = ""
        for name in publics {
            stringPublics += "g" + name + ","
        }
        stringPublics = String(stringPublics.prefix(stringPublics.count - 1))
        print(stringPublics)
        VK.API.NewsFeed.get([.filters: "post", .sourceIds: stringPublics, .count: "100"])
            .onSuccess {
                print("finished")
                if let response = try JSONSerialization.jsonObject(with: $0) as? [String:Any] {
                    ans = []
                    for post in response["items"] as! Array<[String:Any]> {
                        var p: Post = Post()
                        p.publicName = String(post["source_id"] as! Int)
                        p.publicText = post["text"] as! String
                        p.publicationTime = String(post["date"] as! Int)
                        ans.append(p)
                    }
                    DispatchQueue.main.async {
                        completion(ans, feed)
                    }
                }
            }
        .onError {
            error in
            print("Request failed with error: ($0)")
        }
        .send()
        
    }
}
