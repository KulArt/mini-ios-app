//
//  DataManagerProtocol.swift
//  vk_grouper
//
//  Created by Артур Кулапин on 12.05.2021.
//

import Foundation

protocol DataManagerProtocol {
    func getFeed(id: String) -> Array<String>
    func addPublic(userId: String, publicId: String)
}
