//
//  ViewController.swift
//  BasicApp
//
//  Created by Артур Кулапин on 08.05.2021.
//

import UIKit

struct Post {
    var publicName: String = ""
    var publicationTime: String = ""
    var publicText: String = ""
}

class ViewController: UIViewController {
    
    @IBOutlet var addPublicField: UITextField!
    @IBOutlet var addPublicLabel: UILabel!
    
    var currentLink: String = ""
    var publicsInFeed: Array<String> = []
    
    var dataManager = RealmDataManager()
    var vk_manager = VKDelegate()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        view.backgroundColor = .white
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! TableViewController
        dest.vk_manager = self.vk_manager
        dest.dataManager = self.dataManager
    }
    
    @IBAction func viewPublicTapped(_ sender: UIButton) {
        let tableViewController = TableViewController(nibName: "TableViewController", bundle: nil)
        self.present(tableViewController, animated: true, completion: nil)
    }
    
    @IBAction func addPublicTapped(_ sender: UIButton) {
        print("pressed button with adding public")
        let link = addPublicField.text
        if let tmp = vkDelegateReference as? VKDelegate {
            vk_manager.user_id = tmp.user_id
        }
        let userId = vk_manager.user_id
        if link != nil {
            currentLink = link!
            if currentLink.contains("/public") {
                currentLink = String(currentLink.suffix(currentLink.count - "https://vk.com/public".count + 1))
            } else if currentLink.contains("/club") {
                currentLink = String(currentLink.suffix(currentLink.count - "https://vk.com/club".count + 1))
            } else if currentLink.contains("/group") {
                currentLink = String(currentLink.suffix(currentLink.count - "https://vk.com/group".count + 1))
            } else if currentLink.contains("https://vk.com/") {
                currentLink = String(currentLink.suffix(currentLink.count - "https://vk.com/".count))
            } else {
                print("There already exists such public")
            }
        }
        let num = Int(currentLink)
        if num == nil {
            vk_manager.getPublicId(name: currentLink, userId: userId, dataManager: dataManager, completion:{
                ans, userId, dataManager in
                dataManager.addPublic(userId: userId, publicId: ans)
            })
        } else {
            dataManager.addPublic(userId: userId, publicId: currentLink)
        }
        
        addPublicField.text = ""
        print(currentLink)
        publicsInFeed.append(currentLink)
        print(dataManager.getFeed(id: vk_manager.user_id))
    }
}

