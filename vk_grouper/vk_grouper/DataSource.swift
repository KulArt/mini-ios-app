//
//  DataSource.swift
//  vk_grouper
//
//  Created by Артур Кулапин on 12.05.2021.
//

import UIKit
import RealmSwift

class RealmDataSource: NSObject, DataSourceProtocol {
    
    fileprivate let realm = try! Realm()
    
    fileprivate var notificationToken: NotificationToken?
    fileprivate var results: Results<RealmFeed>?
    
    func setup(with tableView: UITableView?, delegate: DataSourceDelegate?) {

        
        results = realm.objects(RealmFeed.self).sorted(byKeyPath: "id")
        
        // Observe Results Notifications
        notificationToken?.invalidate()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
        
        if let person = results?[indexPath.row] {
            cell.textLabel?.text = "\(person.userId) \(person.publicId)"
        }
        
        return cell
    }

    deinit {
        notificationToken?.invalidate()
    }
}
