//
//  DataSourceProtocol.swift
//  vk_grouper
//
//  Created by Артур Кулапин on 12.05.2021.
//

import UIKit

protocol DataSourceProtocol {
    
    func setup(with tableView: UITableView?, delegate: DataSourceDelegate?)
    
}
