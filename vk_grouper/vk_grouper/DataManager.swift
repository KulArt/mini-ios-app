//
//  DataManager.swift
//  vk_grouper
//
//  Created by Артур Кулапин on 12.05.2021.
//

import RealmSwift

class RealmDataManager: DataManagerProtocol {
    
    let realm = try! Realm()
    
    func addPublic(userId: String, publicId: String) {
        if userId == "" || publicId == "" {
            return
        } else {
            let realmFeed = RealmFeed()
            realmFeed.id = userId + "$" + publicId
            realmFeed.userId = userId
            realmFeed.publicId = publicId
            let areSameData: Results<RealmFeed>? = realm.objects(RealmFeed.self).filter("id == %@", realmFeed.id)
            var realmArray: Array<RealmFeed> = []
            if areSameData != nil {
                realmArray = Array(areSameData!)
                if !realmArray.isEmpty {
                    print("Trere is already public: " + publicId)
                    return
                }
            } else {
                return
            }
            try! realm.write {
                realm.add(realmFeed)
            }
        }
    }
    
    
    func getFeed(id: String) -> Array<String> {
        let realmFeed: Results<RealmFeed>? = realm.objects(RealmFeed.self).filter("userId == %@", id)
        var realmArray: Array<RealmFeed> = []
        if realmFeed != nil {
            realmArray = Array(realmFeed!)
        }
        var array: Array<String> = []
        for record in realmArray {
            let p: String = Feed(realm: record).publicId
            array.append(p)
        }
        return array
    }
}
